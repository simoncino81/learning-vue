import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/Home'
import router from '@/router'

describe('Home.vue', () => {
  it('test per vedere se funziona', () => {

  	Vue.use(VueRouter)
    const Constructor = Vue.extend(Home)
    const vm = new Constructor({router}).$mount()
    expect(vm.$el.querySelector('.homeCss h1').textContent)
      .to.equal('Esercizi per imparare vue.js')
  })
})
