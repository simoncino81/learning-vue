import Vue from 'vue'
import VueRouter from 'vue-router'
import SimpleInput from '@/components/exercise/SimpleInput'
import router from '@/router'

describe('SimpleInput.vue', () => {
  it('Sono il primo it fatto da me guarda cose', () => {
  	Vue.use(VueRouter)
    const Constructor = Vue.extend(SimpleInput)
    const vm = new Constructor({router}).$mount()
    vm.$el.querySelector('#inp1').value = 'dio';
    expect(vm.$el.querySelector('label[for="inp1"]').textContent)
      .to.equal('A cannone')
  })
})
