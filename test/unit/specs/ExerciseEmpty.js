import Vue from 'vue'
import VueRouter from 'vue-router'
import ExerciseEmpty from '@/components/exercise/ExerciseEmpty'
import router from '@/router'

describe('ExerciseEmpty.vue', () => {
  it('ExerciseEmpty non deve visualizzare niente', () => {
  	Vue.use(VueRouter)
    const Constructor = Vue.extend(ExerciseEmpty)
    const vm = new Constructor({router}).$mount()
    /*expect(vm.$el.querySelector('.fieldCss legend').textContent)
      .to.notEqual('s')*/
  })
})
