import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Home from '@/components/Home'
import SimpleInput from '@/components/exercise/SimpleInput'
import Select from '@/components/exercise/Select'
import TableCRUD from '@/components/exercise/TableCRUD'
import TransitionEffects from '@/components/exercise/TransitionEffects'
import AjaxCalls from '@/components/exercise/AjaxCalls'
import VuexExamples from '@/components/exercise/VuexExamples'

Vue.use(Router)

export default new Router({
  routes: [
  {
    path: '/hello',
    name: 'Hello',
    component: Hello
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
   path: '/simple-input',
   name: 'SimpleInput',
   component: SimpleInput
 },
 {
  path: '/table-crud',
  name: 'TableCRUD',
  component: TableCRUD
},
{
  path: '/transition-effects',
  name: 'TransitionEffects',
  component: TransitionEffects
},
{
  path: '/ajax-calls',
  name: 'AjaxCalls',
  component: AjaxCalls
},
{
  path: '/vuex-examples',
  name: 'VuexExamples',
  component: VuexExamples
},
{
  path: '/select',
  name: 'Select',
  component: Select
}
]
})
