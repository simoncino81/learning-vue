import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'Axios'

import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)
const state = {
	utenti: []
}

const getters = {
	countUtenti : state => state.utenti.length,
	utenti : state => state.utenti,
	getUtenteById : (state, getters) => (id) => {
		return state.utenti.find(utente => utente.id === id);
	}
}

const mutations = {
	setUtenti : function(state, utenti){
		//console.log('mutations - > setUtenti', utenti)
		state.utenti = utenti;
	},
	deleteUtente : function(state, id){
		//console.log('mutations - > deleteUtente', id)
		let index = null;
		for(let i=0; i<state.utenti.length; i++){
			let el = state.utenti[i];
			if(el.id === id){
				index = i;
				break;
			}
		}
		if(index != null)
			state.utenti.splice(index, 1);
	},
	updateUtente : function(state, utenteUpd){
		//console.log('vuex - mutations - updateUtente', utenteUpd)
		let u = state.utenti.find(utente => utente.id === utenteUpd.id);	
		//console.log('vuex - mutations - updateUtente - u', u)
		u.nome = utenteUpd.nome;

	},
	addUtente : function (state, u) {
		//console.log('vuex - mutations - addUtente', u)
		let uToAdd = {
			id : u.id,
			nome: u.nome,
			twitter: u.twitter,
			photo: u.photo
		}
		state.utenti.push(uToAdd);
	}
}

const actions = {
	loadUtenti : function(commit, state){
		axios.get('http://Simone:3000/json/user.json')
		.then(response => {
			//console.log('actions -> loadUtenti -> response', response);
			store.commit('setUtenti', response.data)
		})
		.catch(err => {
			console.error('actions -> loadUtenti -> error', err)
		})
	},
	updateUtente : function({commit, state}, utenteUpd){
		//console.log('vuex - actions - updateUtente', utenteUpd)
		
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				let esito = {success:false, msg:''}
				store.commit('updateUtente', utenteUpd)
				esito.success = true;
				esito.msg = 'Modificato con successo';
				resolve(esito)
			}, 500)
		})
	},
	deleteUtente : function({commit, state}, id){
		//console.log('vuex - actions - deleteUtente', id)
		store.commit('deleteUtente', id)
		let esito = {success:false, msg:''}
		esito.success = true;
		esito.msg = 'Correttamente eliminato';
		return esito;
	},
	addUtente : function({commit, state}, u){
		//console.log('vuex - actions - addUtente', u)
		store.commit('addUtente', u)
	}
}



const store = new Vuex.Store({
	strict: process.env.NODE_ENV !== 'production',
	state,
	getters,
	actions,
	mutations,
	plugins: [createLogger()]
})

export default store;