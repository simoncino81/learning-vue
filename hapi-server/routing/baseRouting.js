const basedir = '../dist';

module.exports = [
	{ 
		method: 'GET', 
		path: '/', 
		handler: function (request, reply) {
			reply.file(basedir + '/index.html');
		} 
	}
	,{ 
		method: 'GET', 
		path: '/static/css/{name}', 
		handler: function (request, reply) {
			reply.file(basedir + "/static/css/" + request.params.name);
		} 
	}
	,{ 
		method: 'GET', 
		path: '/static/js/{name}', 
		handler: function (request, reply) {
			reply.file(basedir + "/static/js/" + request.params.name);
		} 
	}
];
