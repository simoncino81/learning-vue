const basedir = '../json';

module.exports = [
{ 
	method: 'GET', 
	path: '/json/{name}', 
	handler: function (request, reply) {
		const users = require(basedir + "/" + request.params.name);
		//reply.file(basedir + "/" + request.params.name);
		reply(users);
	} 
}
];
